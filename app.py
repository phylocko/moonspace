import cocos
from enum import Enum
from cocos import actions as a
from cocos.director import director
from random import randint, choice


class Directions(Enum):
    UP = 1,
    DOWN = 2,
    LEFT = 3,
    RIGHT = 4,
    PARK = 0


class Space(cocos.layer.Layer):
    is_event_handler = True

    def __init__(self):
        super().__init__()

        self.keyboard_active = True

        # Space background
        self.background = cocos.sprite.Sprite('res/space.png')
        self.background.scale = 0.6
        self.background.position = self.background.width / 2, self.background.height / 2
        self.add(self.background)
        self.background.do(a.Repeat(a.ScaleTo(0.7, 10) + a.ScaleTo(0.6, 10)))

        self.earth = Earth()
        self.add(self.earth, 3)

        self.position_grid = {}
        self.moons = {}

        self.label = None

        cols_count = director.window.width // self.earth.move_step
        rows_count = director.window.height // self.earth.move_step

        self.position_grid = [(x * self.earth.move_step, y * self.earth.move_step)
                              for x in range(1, cols_count)
                              for y in range(1, rows_count)]

        # Game process
        self.steps = 0
        self.level = 1

        self.populate_moons()

    def populate_moons(self):

        moon_count = 2 ** self.level

        if moon_count > len(self.position_grid):
            self.finish_game()
            return

        i = 0
        while i < moon_count:
            random_position = choice(self.position_grid)
            if self.moons.get(random_position):
                continue
            self.add_moon(random_position, smoothly=True)
            i += 1

    def finish_game(self):
        if self.label:
            self.remove(self.label)
        self.keyboard_active = False
        self.earth.do(a.FadeOut(6) | a.ScaleTo(0, duration=6))
        self.label = cocos.text.Label(
            'Mission completed!',
            font_name='Arial',
            font_size=60,
            anchor_x='center', anchor_y='center'
        )
        self.label.position = director.window.width / 2, director.window.height / 2
        self.label.scale = 1.6
        self.label.opacity = 0
        self.add(self.label)
        self.label.do(a.FadeIn(6) | a.ScaleTo(1, duration=6))

    def add_moon(self, position, smoothly=False):
        moon = Moon()
        moon.position = position
        self.moons[position] = moon
        if smoothly:
            moon.opacity = 0
            self.add(moon)
            moon.do(a.FadeIn(0.5))
        else:
            self.add(moon, 1)

    def remove_moon(self, position):
        moon = self.moons.get(position)
        if moon:
            del (self.moons[position])
            self.remove(moon)

    def toggle_moon(self, position):
        if self.moons.get(position):
            self.remove_moon(position)
        else:
            self.add_moon(position)

    def finish_level(self):
        self.label = cocos.text.Label(
            'You saved the world in %s steps!' % (self.steps),
            font_name='Arial',
            font_size=32,
            anchor_x='center', anchor_y='center'
        )
        self.label.position = director.window.width / 2, director.window.height / 2

        self.label.opacity = 0
        self.add(self.label, 4)
        self.label.do(a.FadeIn(0.3))

        self.level += 1
        self.steps = 0
        self.populate_moons()

    def on_key_press(self, key, modifiers):
        if not self.keyboard_active:
            return

        if self.label:
            self.remove(self.label)
            self.label = None

        # UP
        if key == 65362:
            self.earth.direction = Directions.UP

        elif key == 65364:
            self.earth.direction = Directions.DOWN

        elif key == 65361:
            self.earth.direction = Directions.LEFT

        elif key == 65363:
            self.earth.direction = Directions.RIGHT

        elif key == 32:
            self.earth.direction = Directions.PARK


class Moon(cocos.sprite.Sprite):
    def __init__(self):
        super().__init__('res/moon.png')
        self.scale = 0.1
        if randint(0, 10) == 5:
            self.scale = 0.15
        self.do(a.Repeat(a.Rotate(360, 10)))


class Earth(cocos.sprite.Sprite):

    def __init__(self):
        super(Earth, self).__init__('res/earth.png')

        self.speed = 0.5  # seconds
        self.direction = Directions.PARK

        self.position = 500, 300
        self.velocity = (0, 0)
        self.scale = 0.2
        self.move_step = 50
        self.managed = True

        self.shifts = {
            Directions.UP: (0, self.move_step),
            Directions.DOWN: (0, -self.move_step),
            Directions.LEFT: (-self.move_step, 0),
            Directions.RIGHT: (self.move_step, 0),
        }
        self.inverted_directions = {
            Directions.UP: Directions.DOWN,
            Directions.DOWN: Directions.UP,
            Directions.LEFT: Directions.RIGHT,
            Directions.RIGHT: Directions.LEFT,
        }

        self.schedule_interval(self.move, self.speed)

        self.previous_position = None

        self.do(a.Repeat(a.Rotate(360, 5)))

    def move(self, *args, **kwargs):

        if not self.parent.keyboard_active:
            return

        if self.direction == Directions.PARK:
            return

        shift = self.shifts.get(self.direction, (0, 0))

        next_position = (self.position[0] + shift[0], self.position[1] + shift[1])

        if next_position[0] <= self.move_step / 2 or next_position[1] <= self.move_step / 2 \
                or next_position[0] >= director.window.width - self.move_step / 2 \
                or next_position[1] >= director.window.height - self.move_step / 2:
            self.direction = self.inverted_directions.get(self.direction, Directions.PARK)
            next_position = self.previous_position

        self.previous_position = self.position

        self.parent.toggle_moon(self.position)
        if self.parent.moons:
            self.do(a.MoveTo(next_position, 0.2))
            self.parent.steps += 1
        else:

            self.direction = Directions.PARK
            self.parent.finish_level()
            return


if __name__ == "__main__":
    director.init(width=1000, height=500, autoscale=False)
    main_scene = cocos.scene.Scene(Space())
    director.run(main_scene)
