**Installation and run**

1. git clone https://gitlab.com/phylocko/moonspace.git
2. cd moonspace/
3. virtualenv ~/envs/moonspace
4. source ~/envs/moonspace/bin/activate
5. pip install requirements.pip
6. python app.py

 